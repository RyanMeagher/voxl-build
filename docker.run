#!/bin/bash
################################################################################
# Copyright 2019 ModalAI Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# 4. The Software is used solely in conjunction with devices provided by
#    ModalAI Inc.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
################################################################################


function printUsage() {
    cat <<EOF
Run the docker with the right options
Usage: sudo $(basename $0) [OPTIONS]
OPTIONS:
  -h: Help
  -d <name>: The name of the directory to mount inside the docker
  -i <name>: The name of the docker image to run
EOF
    exit 1
}

optDir=
optImg=

while getopts 'hd:i:' opt
do
    case $opt in
	h)
	    printUsage
	    ;;
	d)
	    optDir="$OPTARG"
	    ;;
	i)
	    optImg="$OPTARG"
	    ;;
	*)
	    printUsage
	    ;;
    esac
done
if [ -z ${optDir} ]
then
    echo "[ERROR] Required argument for directory to mount: -d"
    exit 1
fi

if [ -z ${optImg} ]
then
    echo "[ERROR] Required argument for docker image: -i"
    exit 1
fi
set -x
docker run \
   -it \
   --net=host \
   --mount "type=bind,src=${optDir},dst=${optDir}" \
   --volume="$HOME/.Xauthority:/root/.Xauthority:rw" \
   --privileged \
   --cap-add=SYS_ADMIN \
   --security-opt apparmor:unconfined \
   --env="DISPLAY" \
   -u user \
   -w /home/user ${optImg} /bin/bash 
