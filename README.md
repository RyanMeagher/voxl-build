# VOXL Open Source Kernel Build

The open-source only components of the VOXL code can now be built independently of the proprietary binaries. This enables users to configure and build their own kernels, and develop/modify device drivers without requiring any proprietary code.

The build system used to build the kernel and the kmods is Yocto: https://www.yoctoproject.org/docs/1.6/ref-manual/ref-manual.html


<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Setup](#setup)
  - [Install Docker](#install-docker)
  - [Clone this Project](#clone-this-project)
  - [Build the Docker Image](#build-the-docker-image)
- [Build Guides](#build-guides)
  - [Run the Docker Image](#run-the-docker-image)
  - [Building the Kernel and WLAN Module](#building-the-kernel-and-wlan-module)
  - [Kernel and WLAN Module File Location](#kernel-and-wlan-module-file-location)
- [Using the Kernel](#using-the-kernel)
  - [Testing the Kernel Before Flashing](#testing-the-kernel-before-flashing)
  - [Flashing the Kernel](#flashing-the-kernel)
  - [Installing the Wi-Fi Kernel Module](#installing-the-wi-fi-kernel-module)
- [Modifying the Kernel](#modifying-the-kernel)
  - [Using meta-voxl](#using-meta-voxl)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->


# Setup

**NOTE**: These instructions have been tested on build hosts running Ubuntu 14.04 and Ubuntu 18.04.

## Install Docker

Follow instructions here: https://docs.docker.com/install/linux/docker-ce/ubuntu/

## Clone this Project

```bash
git clone https://gitlab.com/voxl-public/voxl-build.git
cd voxl-build
```

## Build the Docker Image

Assuming the Docker image tag you'll use is called `voxl-build`, run the following to create the image:

```bash
docker build -t voxl-build -f ubuntu-xenial.Dockerfile .
```

Notes About the Docker Image:

- The dockerfile installs all the necessary packages for doing the build
- It does all the necessary configuration
- It downloads and installs AOSP's `repo`tool
- It creates a non-root user with username `user` and password `user`
  - This user has sudo privileges
- It installs `/home/user/build.sh` that sets up the rest of the build environment inside the docker. This script does a few things:
  - repo init, followed by repo sync, of the open source repositories
  - Patch the code so it can be built independent of any proprietary code
  - Source the `set_bb_env.sh` script and set up environment variables needed to do the build
  - Runs the build


# Build Guides

## Run the Docker Image

Assuming your workspace is on your host system is `/opt/data/workspace`, start a Docker container that mounts this directory at the same location inside the Docker:

```bash
sudo mkdir -p /opt/data/workspace
./docker.run -d /opt/data/workspace -i voxl-build
```

Notes

- We mount `/opt/data/workspace` from the build host to the docker container using `--mount`
- We run as account `user`, in her home directory `/home/user`
- The `docker.run` script just calls `docker run` with the right arguments. Look inside the script for details.

## Building the Kernel and WLAN Module

At this point you should be inside the `voxl-build` Docker container.

**Important** Before running the build script, change directory to your mounted directory so you can access the files outside of Docker and change ownership so the script can build in it.

```bash
cd /opt/data/workspace
sudo chown user . 
# sudo password is 'user'
```

Now, run the build script, specifying the build directory that is mounted

```bash
source /home/user/build.sh /opt/data/workspace
```

The build will start and will generally take a **long time** on a standard workstation...

## Kernel and WLAN Module File Location

Once completed, the Kernel and WLAN kernel module will be located at:

```bash
/opt/data/workspace/poky/build/tmp-glibc/deploy/images/apq8096-drone/apq8096-boot.img
```
```bash
/opt/data/workspace/poky/build/tmp-glibc/deploy/images/apq8096-drone/kernel_modules/qcacld-ll/wlan.ko
```

# Using the Kernel

## Testing the Kernel Before Flashing

You can test the kernel before flashing it.  First, let's get the current kernel version.  Have the device connected with USB and run the following (you'll likely have a different timestamp here...)

```bash
$ adb shell uname -a
Linux apq8096 3.18.71-perf #1 SMP PREEMPT Wed Nov 14 00:43:07 UTC 2018 aarch64 GNU/Linux
```

Let's now boot the device using the new kernel:

```bash
$ adb reboot bootloader
$ fastboot devices
45ef3224	fastboot
```
```bash
$ fastboot boot /opt/data/workspace/poky/build/tmp-glibc/deploy/images/apq8096-drone/apq8096-boot.img
downloading 'boot.img'...
OKAY [  1.849s]
booting...
OKAY [  0.358s]
finished. total time: 2.208s
```

Check that the timestamp changed:

```bash
$ adb devices
List of devices attached
45ef3224	device
$ adb shell uname -a
Linux apq8096 3.18.71-perf #1 SMP PREEMPT Wed Mar 13 21:46:37 UTC 2019 aarch64 GNU/Linux
```
- If the device doesn't boot, or doesn't show the newly built kernel in use, there's something wrong with the newly built kernel. But you did no permanent damage, the old kernel will still be usable and the device just needs to be power cycled.

## Flashing the Kernel

If the device boots fine and everything looks good with the new kernel, proceed to the next step to flash this image onto the device and use it persistently

```bash
$ adb reboot bootloader
$ fastboot devices
45ef3224	fastboot
```
```bash
$ fastboot flash boot /opt/data/workspace/poky/build/tmp-glibc/deploy/images/apq8096-drone/apq8096-boot.img
downloading 'boot.img'...
OKAY [  1.849s]
booting...
OKAY [  0.358s]
finished. total time: 2.208s
```
```bash
$ adb devices
List of devices attached
45ef3224	device
$ adb shell uname -a
Linux apq8096 3.18.71-perf #1 SMP PREEMPT Wed Mar 13 21:46:37 UTC 2019 aarch64 GNU/Linux
```

## Installing the WLAN Kernel Module

After flashing the kernel, you will need to install the WLAN kernel module to `/lib/modules/3.18.71-perf/extra/wlan.ko`.  From the host computer, use the following to copy the file to the target.

```bash
adb push /opt/data/workspace/poky/build/tmp-glibc/deploy/images/apq8096-drone/kernel_modules/qcacld-ll/wlan.ko /lib/modules/3.18.71-perf/extra/wlan.ko
```

# Modifying the Kernel

For an example of modifying the kernel config, see the meta-voxl layer: https://gitlab.com/voxl-public/meta-voxl.

## Using meta-voxl

- Clone the meta-voxl git project under the `poky` directory
- cd poky
- Source `build/conf/set_bb_env.sh'
- Rebuild kernel
